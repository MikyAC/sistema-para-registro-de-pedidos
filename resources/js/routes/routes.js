import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home'
import SignIn from '../pages/SignIn'
import SignUp from '../pages/SignUp'
import Dashboard from '../pages/Dashboard'
import Posts from '../pages/Posts'
import CreatePost from '../pages/CreatePost'
import UserPosts from '../pages/UserPosts'
import DetailPost from '../pages/DetailPost'
import EditPost from '../pages/EditPost'
import Pedidos from '../pages/Pedidos'
import Tiendas from '../pages/Tiendas'
import CreateCategory from '../pages/CreateCategory'
import AllCategory from '../pages/AllCategory'
import AllTiendas from '../pages/AllTiendas'
import AllPedidos from '../pages/AllPedidos'




Vue.use(VueRouter)

const routes = [
    { path: '/', name: 'home', component: Home },
    { path: '/signin', name: 'signin', component: SignIn },
    { path: '/signup', name: 'signup', component: SignUp },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        children: [{
                path: 'post',
                name: 'dashboard.post',
                component: Posts
            },
            {
                path: 'post-create',
                name: 'dashboard.createpost',
                component: CreatePost
            },
            {
                path: 'user-post',
                name: 'dashboard.userpost',
                component: UserPosts
            },
            {
                path: 'detail-post/:slug',
                name: 'dashboard.detailpost',
                component: DetailPost
            },
            //para editar
            {
                path: 'edit-post/:slug',
                name: 'dashboard.editpost',
                component: EditPost
            },
            //para mis tiendas
            {
                path: 'tiendas-create',
                name: 'dashboard.tiendacreate',
                component: Tiendas
            },
            //para mis pedidos
            {
                path: 'pedido-create',
                name: 'dashboard.pedidocreate',
                component: Pedidos
            },
            //para crear categoruas
            {
                path: 'category-create',
                name: 'dashboard.createCategory',
                component: CreateCategory
            },
            //para ver categorias
            {
                path: 'category-all',
                name: 'dashboard.allcategory',
                component: AllCategory
            },
            //para ver todas las tiendas
            {
                path: 'tiendas-all',
                name: 'dashboard.allcatetiendas',
                component: AllTiendas
            },
            //para ver los pedidos
            {
                path: 'pedidos-all',
                name: 'dashboard.allpedidos',
                component: AllPedidos
            },
        ]
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router