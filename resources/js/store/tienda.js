import axios from 'axios'

export default {
    namespaced: true,
    state: {
        alltiendas: []
    },
    getters: {
        get_all_tiendas(state) {
            return state.alltiendas
        }
    },
    mutations: {
        SET_ALL_TIENDA(state, value) {
            state.alltiendas = value
        }
    },
    actions: {
        async getTiendasAll({ dispatch }) {
            console.log('solicitando todas las tiendas')
            await axios.get('/sanctum/csrf-cookie')
            const ct = await axios.get('/api/tiendas')
            return dispatch('metiendas', ct.data)
        },
        async crearTienda({ dispatch }, newTienda) {
            console.log('creando una nueva newTienda')
            console.log(newTienda)
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/tiendas', newTienda)

        },
        metiendas({ commit }, data) {
            console.log(data)
            commit('SET_ALL_TIENDA', data)
        }
    }
}