import axios from 'axios'

export default {
    namespaced: true,
    state: {
        allpedidos: []
    },
    getters: {
        get_all_pedidos(state) {
            return state.allpedidos
        }
    },
    mutations: {
        SET_ALL_PEDIDO(state, value) {
            state.allpedidos = value
        }
    },
    actions: {
        async getPedidosAll({ dispatch }) {
            console.log('solicitando todas los pedidos')
            await axios.get('/sanctum/csrf-cookie')
            const ct = await axios.get('/api/pedidos')
            return dispatch('mepedidos', ct.data)
        },
        async crearTienda({ dispatch }, newTienda) {
            console.log('creando una nueva newTienda')
            console.log(newTienda)
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/tiendas', newTienda)

        },
        mepedidos({ commit }, data) {
            console.log(data)
            commit('SET_ALL_PEDIDO', data)
        }
    }
}