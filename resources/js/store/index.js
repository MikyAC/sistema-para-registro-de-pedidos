import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import post from './post'
import category from './category'
import tienda from './tienda'
import pedidos from './pedidos'

Vue.use(Vuex)
export default new Vuex.Store({
    modules: {
        auth,
        post,
        category,
        tienda,
        pedidos
        //aqui los demas modulos
    }
})