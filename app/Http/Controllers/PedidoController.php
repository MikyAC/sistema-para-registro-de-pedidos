<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\Post;
use App\Models\Tienda;
use Illuminate\Http\Request;
use App\Models\User;
class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $data =  $request->all();
        $tienda = '';
        $dir_entrega = '';

        foreach($data as $key => $item){

                if($key == 0){
                    $tiendaModel = Tienda::where('id',(int)$item['value'])->first();
                    $tienda = $tiendaModel->nombre;
                    $dir_entrega = $tiendaModel->direccion;
                }else{
                    $productoModel = Post::where('id',(int)$item['name'])->first();    
                    $producto = $productoModel->title;
                    $cantidad = $item['value'];
                    $precio_total = (int)$item['value'] *  $productoModel->precio;
                    
                    //dd($nuevopedido);
                    Pedido::create([
                        'tienda' =>  $tienda,
                        'dir_entrega' =>  $dir_entrega,
                        'producto' =>  $producto,
                        'cantidad' =>  $cantidad,
                        'precio_total' =>  $precio_total,
                    ]);
                    /*if($item['name'] == 'tienda')
    
                    }else{
                            //$item['name'] id del producto
                        //$item['value'] la ccantidad que te estan pidiendo
                
                        error_log('el producto '.$item['name'].'quieren '.$item['value'].' productos');   
                    }*/
                }
            
        }

        return response()->json(['state'=>'Exito de pedido'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function show(Pedido $pedido)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function edit(Pedido $pedido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pedido $pedido)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pedido $pedido)
    {
        //
    }
}

