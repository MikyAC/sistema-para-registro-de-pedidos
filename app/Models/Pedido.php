<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\user;

class Pedido extends Model
{
    use HasFactory;

    protected $fillable = [
        'tienda',
        'dir_entrega',
        'producto',
        'cantidad',
        'precio_total'
    ];
}
